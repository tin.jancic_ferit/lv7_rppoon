﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZAD1;
namespace ZAD2
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] sequence = { 12.2, 1.65, 4.3, 2.6, 15.0, 13.54, 64.3, 0.0, 45.3, 23.5 };
            BubbleSort bubble = new BubbleSort();
            LinearSearch linear = new LinearSearch();

            NumberSequence numberSequence = new NumberSequence(sequence);

            numberSequence.SetSearchStrategy(linear);
            numberSequence.SetX(12.2);
            Console.Write("Polje prije sortiranja: \n" + numberSequence.ToString());
            Console.Write(" => Index trazenog broja prije sortiranja: ");
            Console.Write(numberSequence.Search().ToString() + "\n");

            numberSequence.SetSortStrategy(bubble);
            numberSequence.Sort();
            Console.Write("Polje nakon sortiranja: \n" + numberSequence.ToString());
            Console.Write(" => Index trazenog broja nakon sortiranja: ");
            Console.Write(numberSequence.Search().ToString() + "\n");
        }
    }
}
