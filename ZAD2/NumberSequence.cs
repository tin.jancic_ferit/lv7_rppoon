﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZAD2;
namespace ZAD1
{
    class NumberSequence
    {
        private double[] sequence;
        private int sequenceSize;
        private double x;
        private SortStrategy sortStrategy;
        private SearchStrategy searchStrategy;

        
        public NumberSequence(int sequenceSize)
        {
            this.sequenceSize = sequenceSize;
            this.sequence = new double[sequenceSize];
            this.x = 0;
        }
        public NumberSequence(double[] array) : this(array.Length)
        {
            array.CopyTo(this.sequence, 0);
        }
        public void SetX(double x)
        {
            this.x = x;
        }
        
        public void InsertAt(int index, double value)
        {
            this.sequence[index] = value;
        }
        public void SetSortStrategy(SortStrategy strategy)
        {
            this.sortStrategy = strategy;
        }
        public void SetSearchStrategy(SearchStrategy strategy)
        {
            this.searchStrategy = strategy;
        }
        public void Sort() { this.sortStrategy.Sort(this.sequence); }
        public int Search() => this.searchStrategy.Search(this.sequence, this.x); 
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach(double element in this.sequence)
            {
                builder.Append(element).Append(Environment.NewLine);
            }
            return builder.ToString();
        }
    }
}
