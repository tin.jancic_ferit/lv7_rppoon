﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD1
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] sequence = { 12.2, 1.65, 4.3, 2.6, 15.0, 13.54, 64.3, 0.0, 45.3, 23.5 };
            BubbleSort bubble = new BubbleSort();
            SequentialSort seq = new SequentialSort();
            CombSort comb = new CombSort();
            

            NumberSequence numberSequence = new NumberSequence(sequence);
            Console.Write("Početno polje: " + numberSequence.ToString());
            numberSequence.SetSortStrategy(bubble);
            numberSequence.Sort();
            Console.Write("Bubble: " + numberSequence.ToString());
            numberSequence.SetSortStrategy(seq);
            numberSequence.Sort();
            Console.Write("Seq: " + numberSequence.ToString());
            numberSequence.SetSortStrategy(comb);
            numberSequence.Sort();
            Console.Write("Comb: " + numberSequence.ToString());

        }
    }
}
